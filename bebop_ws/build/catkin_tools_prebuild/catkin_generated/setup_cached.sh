#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/desperado/bebop/bebop_ws/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH='/home/desperado/ros/yolo/devel/lib:/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/usr/local/cuda/lib64:/usr/local/ffmpeg/lib'
export PATH='/opt/ros/kinetic/bin:/usr/local/cuda/bin:/home/desperado/bin:/home/desperado/.local/bin:/usr/local/ffmpeg/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/desperado/.local/bin'
export PWD='/home/desperado/bebop/bebop_ws/build/catkin_tools_prebuild'
export ROSLISP_PACKAGE_DIRECTORIES="/home/desperado/bebop/bebop_ws/devel/.private/catkin_tools_prebuild/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/desperado/bebop/bebop_ws/build/catkin_tools_prebuild:$ROS_PACKAGE_PATH"