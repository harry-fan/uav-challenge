#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export PWD='/home/desperado/bebop/bebop_ws/build/bebop_tools'

# modified environment variables
export CMAKE_PREFIX_PATH="/home/desperado/bebop/bebop_ws/devel/.private/bebop_tools:$CMAKE_PREFIX_PATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/desperado/bebop/bebop_ws/devel/.private/bebop_tools/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/desperado/bebop/bebop_ws/src/bebop_autonomy/bebop_tools:$ROS_PACKAGE_PATH"